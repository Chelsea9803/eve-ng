--se habilita esquema para ORDS se Hace una sola vez
BEGIN
  ORDS.enable_schema(
    p_enabled             => TRUE,
    p_schema              => 'DBMONITOR',
    p_url_mapping_type    => 'BASE_PATH',
    p_url_mapping_pattern => 'SM4',
    p_auto_rest_auth      => FALSE
  );
    
  COMMIT;
END;
/

--TB_SETICPUMON
/*CREATE TABLE "DBMONITOR"."TB_SETICPUMON" 
   (	"IDMONITOR" VARCHAR2(20 BYTE), 
	"HOSTNAME" VARCHAR2(30 BYTE), 
	"DBNAME" VARCHAR2(30 BYTE), 
	"VALUE_NAME" VARCHAR2(30 BYTE), 
	"VALUE_SO" NUMBER, 
	"VALUE_DB" NUMBER, 
	"FECHA_REG" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBS_SETIMONITOR" ;*/
CREATE OR REPLACE PROCEDURE DBMONITOR.PRMON_SETICPUMONREMO (
  P_IDMONITOR     IN  TB_SETICPUMON.idmonitor%TYPE,
  P_HOSTNAME     IN  TB_SETICPUMON.hostname%TYPE,
  P_DBNAME     IN  TB_SETICPUMON.dbname%TYPE,
  P_VALUE_NAME     IN  TB_SETICPUMON.value_name%TYPE,
  P_VALUE_SO     IN  TB_SETICPUMON.value_so%TYPE,
  P_VALUE_DB     IN  TB_SETICPUMON.value_db%TYPE
)
AS
FECHA DATE;
BEGIN
	SELECT SYSDATE INTO FECHA FROM DUAL;
  INSERT INTO DBMONITOR.TB_SETICPUMON (idmonitor, hostname, dbname, value_name, value_so, value_db, fecha_reg)
  VALUES (P_IDMONITOR, P_HOSTNAME,P_DBNAME,P_VALUE_NAME, P_VALUE_SO, P_VALUE_DB, FECHA);
EXCEPTION
  WHEN OTHERS THEN
    HTP.print(SQLERRM);
END;
/

-------------------------
--API consumo de servicio de insert

BEGIN
  ORDS.define_module(
    p_module_name    => 'server',
    p_base_path      => 'server/',
    p_items_per_page => 0);
  
  ORDS.define_template(
   p_module_name    => 'server',
   p_pattern        => 'cpu/');

  ORDS.define_handler(
    p_module_name    => 'server',
    p_pattern        => 'cpu/',
    p_method         => 'POST',
    p_source_type    => ORDS.source_type_plsql,
    p_source         => 'BEGIN
                           PRMON_SETICPUMONREMO(P_IDMONITOR    => :idmonitor,  
                                              P_HOSTNAME    => :hostname,
										                        P_DBNAME    => :dbname,
                                           P_VALUE_NAME    => :value_name,
                                           P_VALUE_SO    => :value_so,
                                           P_VALUE_DB    => :value_db
                                           );
                         END;',
    p_items_per_page => 0);

  COMMIT;
END;
/
---------------------------------------------------Script de monitoreo de CPU sin cliente Oracle
#!/bin/sh
##############################################################################################
## AUTOR          : JORGE CASTRO | DBA SETI S.A.S                                           ##
## FECHA CREACION : 04-04-2024                                                              ##
## OBJETIVO       : MONITOREO REMOTO SIN CLIENTE ORACLE                                     ##
## MODIFICACIONES PROGRAMACION :                                                            ##
##############################################################################################

##############################################################################################
## DEFINICION VARIABLES DE AMBIENTE
##############################################################################################
V_RANDOM=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 20)
HOSTNAME=$(hostname)
CADENA=CPU
##############################################################################################
## FUCION PARA REVISAR CPU
##############################################################################################
fn_CPULINUXDBREMO()
{
VPU=$(vmstat 1 2 | tail -1 | awk '{print $13+$14}')
DBCPU=0
echo "CPU%"$HOSTNAME"%"$VPU
##############################################################################################
## Se debe mapear la ruta del curl si no funciona bien
##############################################################################################
curl -i -X POST -d '{"idmonitor": "'"$V_RANDOM"'", "hostname": "'"$HOSTNAME"'", "dbname": "NA", "value_name": "'"$CADENA"'", "value_so": "'"$VPU"'", "value_db": "'"$DBCPU"'"}' -H "Content-Type: application/json" http://172.16.2.89:8080/ords/SM4/server/cpu/
}
fn_CPULINUXDBREMO 1>/dev/null 2>/dev/nul



####################################################################################################################
## AUTOR          : Jorge Andres Castro - DBA SETI S.A.S                                                          ##
## FECHA CREACION : 04-Abr-2024                                                                                   ##
## OBJETIVO       : Monitoreo recursos de CPU en servidores por servicios REST                                    ##
## NOTIFICACION   : SI                                                                                            ##
##                                                                                                                ##
## MODIFICACIONES :                                                                                               ##
####################################################################################################################
00,05,10,15,20,25,30,35,40,45,50,55 * * * * /home/oracle/seti/bin/fn_CPULINUXDBREMO
00,05,10,15,20,25,30,35,40,45,50,55 * * * * /home/oracle/seti/bin/fn_MEMORYLINUXDBREMO
00,05,10,15,20,25,30,35,40,45,50,55 * * * * /home/oracle/seti/bin/fn_PAGLINUXDBREMO
00,05,10,15,20,25,30,35,40,45,50,55 * * * * /home/oracle/seti/bin/fn_DISKLINUXDBREMO
00,06,12,18,24,30,36,42,48,54 * * * * /home/oracle/seti/bin/fnValidaFilesystemREMO

BEGIN
  ORDS.define_module(
    p_module_name    => 'server',
    p_base_path      => 'server/',
    p_items_per_page => 0);
  
  ORDS.define_template(
   p_module_name    => 'server',
   p_pattern        => 'cpu/');

  ORDS.define_handler(
    p_module_name    => 'server',
    p_pattern        => 'cpu/',
    p_method         => 'POST',
    p_source_type    => ORDS.source_type_plsql,
    p_source         => 'BEGIN
                           PRMON_SETICPUMONREMO(P_IDMONITOR    => :idmonitor,  
                                              P_HOSTNAME    => :hostname,
										                        P_DBNAME    => :dbname,
                                           P_VALUE_NAME    => :value_name,
                                           P_VALUE_SO    => :value_so,
                                           P_VALUE_DB    => :value_db
                                           );
                         END;',
    p_items_per_page => 0);

    ORDS.define_template(
   p_module_name    => 'server',
   p_pattern        => 'mem/');

  ORDS.define_handler(
    p_module_name    => 'server',
    p_pattern        => 'mem/',
    p_method         => 'POST',
    p_source_type    => ORDS.source_type_plsql,
    p_source         => 'BEGIN
                           PRMON_SETIMEMMONREMO(P_IDMONITOR    => :idmonitor,  
                                              P_HOSTNAME    => :hostname,
										                        P_DBNAME    => :dbname,
                                           P_VALUE_NAME    => :value_name,
                                           P_MEMORY_TOT    => :memory_tot,
                                           P_MEMORY_USED    => :memory_used,
                                           P_MEMORY_FREE    => :mermory_free
                                           );
                         END;',
    p_items_per_page => 0);

    ORDS.define_template(
   p_module_name    => 'server',
   p_pattern        => 'paging/');

  ORDS.define_handler(
    p_module_name    => 'server',
    p_pattern        => 'paging/',
    p_method         => 'POST',
    p_source_type    => ORDS.source_type_plsql,
    p_source         => 'BEGIN
                           PRMON_SETIPAGINGMONREMO(P_IDMONITOR    => :idmonitor,  
                                              P_HOSTNAME    => :hostname,
										                        P_DBNAME    => :dbname,
                                           P_VALUE_NAME    => :value_name,
                                           P_PAGING    => :paging
                                           );
                         END;',
    p_items_per_page => 0);

    ORDS.define_template(
   p_module_name    => 'server',
   p_pattern        => 'disk/');

  ORDS.define_handler(
    p_module_name    => 'server',
    p_pattern        => 'disk/',
    p_method         => 'POST',
    p_source_type    => ORDS.source_type_plsql,
    p_source         => 'BEGIN
                           PRMON_SETIDISKMONREMO(P_IDMONITOR    => :idmonitor,  
                                              P_HOSTNAME    => :hostname,
										                        P_DBNAME    => :dbname,
                                           P_VALUE_NAME    => :value_name,
                                           P_VALUE_SO    => :value_so
                                           );
                         END;',
    p_items_per_page => 0);

    ORDS.define_template(
   p_module_name    => 'server',
   p_pattern        => 'fs/');

  ORDS.define_handler(
    p_module_name    => 'server',
    p_pattern        => 'fs/',
    p_method         => 'POST',
    p_source_type    => ORDS.source_type_plsql,
    p_source         => 'BEGIN
                           PRMON_SETIFSMONREMO(P_IDMONITOR    => :idmonitor,
                                           P_HOSTNAME    => :hostname,
										   P_FSNAME    => :fsname,
                                           P_FSSIZE    => :fssize,
                                           P_FSFREE    => :fsfree,
                                           P_FSPCT    => :fspct
                                           );
                         END;',
    p_items_per_page => 0);

  COMMIT;
END;
/